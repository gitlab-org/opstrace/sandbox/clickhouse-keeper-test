# ClickHouse Keeper Test

Check that ClickHouse Keeper can be used with the Altinity ClickHouse operator.

See https://clickhouse.com/docs/en/operations/clickhouse-keeper/.

You can use [asdf](http://asdf-vm.com/) to install the project dependencies.

## Makefile commands

- `make setup-minikube` - create a Minikube instance with the latest ClickHouse operator installed in `kube-system`.
- `make install-clickhouse` - create a `ClickHouseInstallation` set up with Keeper replication.
- `make install-replia-test` - installs a two pod deployment, one to produce records into a replica, another to read.

## Testing

Eventually the installed cluster of 3 nodes should stabilise using raft consensus. Check the logs of each node to verify.

The `test.events_local` (`ReplicatedMergeTree` engine) and `test.events` (`Distributed` engine) tables should exist on each node.

Run `make install-replica-test` to create a deployment which will write to replica 0 and read from replica 2. Check the logs of each container.
The producer logs should be empty while ever it can connect and write. The consumer logs should constantly print the most recent 2 records from the distributed events table.

Try deleting one of the replicas. The cluster should recover, as should the producer/consumer containers.

## Findings

The Altinity ClickHouse operator doesn't work well with the Keeper raft configuration by default.

It's useful to look at the operator [creator.go](https://github.com/Altinity/clickhouse-operator/blob/dc6cdc6f2f61fc333248bb78a8f8efe792d14ca2/pkg/model/creator.go)
to view the defaults for various types.

We've made the following adjustments to get this working:

- `keeper_server.server_id` and `raft_configuration.server.id` need a numeric id. We can pull this into an environment variable from the `clickhouse.altinity.com/replica` pod label.
- `serviceTemplate` needs addition of the Keeper ZooKeeper compatible port (2181) and the raft consensus protocol port (9444).
- `configuration.zookeeper` is still required so that ClickHouse replication can talk to Keeper. Here we just reference the exposed `localhost` port of the built in keeper.
- `readinessProbe`/`livenessProbe` are disabled at the moment to allow the operator to scale the cluster before the raft consensus can be formed.
- `CLICKHOUSE_INIT_TIMEOUT` environment variable is required if using any init.db scripts (e.g. to create a database) to increase init timeout (default is 12 checks).