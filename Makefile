setup-minikube:
	minikube start
	-kubectl apply -f https://raw.githubusercontent.com/Altinity/clickhouse-operator/master/deploy/operator/clickhouse-operator-install-bundle.yaml

install-clickhouse:
	kubectl apply -f clickhouse-installation.yaml

delete-clickhouse:
	kubectl delete -f clickhouse-installation.yaml

install-replia-test:
	kubectl apply -f replica-test-deployment.yaml

.PHONY: setup-minikube install-clickhouse